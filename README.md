# Recipe Manager #

Collect and schedule recipes all in one application. Built using Javascript and CSS. Launched as a native app using Electron.

![Recipe Manager](https://i.imgur.com/izeGNlQ.jpg)
